-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module Api.Hasura.Enum.Area_update_column exposing (..)

import Json.Decode as Decode exposing (Decoder)


{-| update columns of table "area"

  - Id - column name
  - Name - column name

-}
type Area_update_column
    = Id
    | Name


list : List Area_update_column
list =
    [ Id, Name ]


decoder : Decoder Area_update_column
decoder =
    Decode.string
        |> Decode.andThen
            (\string ->
                case string of
                    "id" ->
                        Decode.succeed Id

                    "name" ->
                        Decode.succeed Name

                    _ ->
                        Decode.fail ("Invalid Area_update_column type, " ++ string ++ " try re-running the @dillonkearns/elm-graphql CLI ")
            )


{-| Convert from the union type representing the Enum to a string that the GraphQL server will recognize.
-}
toString : Area_update_column -> String
toString enum____ =
    case enum____ of
        Id ->
            "id"

        Name ->
            "name"


{-| Convert from a String representation to an elm representation enum.
This is the inverse of the Enum `toString` function. So you can call `toString` and then convert back `fromString` safely.

    Swapi.Enum.Episode.NewHope
        |> Swapi.Enum.Episode.toString
        |> Swapi.Enum.Episode.fromString
        == Just NewHope

This can be useful for generating Strings to use for <select> menus to check which item was selected.

-}
fromString : String -> Maybe Area_update_column
fromString enumString____ =
    case enumString____ of
        "id" ->
            Just Id

        "name" ->
            Just Name

        _ ->
            Nothing
