-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module Api.Hasura.Object.Initiative_max_fields exposing (..)

import Api.Hasura.InputObject
import Api.Hasura.Interface
import Api.Hasura.Object
import Api.Hasura.Scalar
import Api.Hasura.ScalarCodecs
import Api.Hasura.Union
import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode


contact : SelectionSet (Maybe String) Api.Hasura.Object.Initiative_max_fields
contact =
    Object.selectionForField "(Maybe String)" "contact" [] (Decode.string |> Decode.nullable)


corporate_partners : SelectionSet (Maybe String) Api.Hasura.Object.Initiative_max_fields
corporate_partners =
    Object.selectionForField "(Maybe String)" "corporate_partners" [] (Decode.string |> Decode.nullable)


description : SelectionSet (Maybe String) Api.Hasura.Object.Initiative_max_fields
description =
    Object.selectionForField "(Maybe String)" "description" [] (Decode.string |> Decode.nullable)


funding_institution : SelectionSet (Maybe String) Api.Hasura.Object.Initiative_max_fields
funding_institution =
    Object.selectionForField "(Maybe String)" "funding_institution" [] (Decode.string |> Decode.nullable)


funding_year : SelectionSet (Maybe Int) Api.Hasura.Object.Initiative_max_fields
funding_year =
    Object.selectionForField "(Maybe Int)" "funding_year" [] (Decode.int |> Decode.nullable)


id : SelectionSet (Maybe Api.Hasura.ScalarCodecs.Uuid) Api.Hasura.Object.Initiative_max_fields
id =
    Object.selectionForField "(Maybe ScalarCodecs.Uuid)" "id" [] (Api.Hasura.ScalarCodecs.codecs |> Api.Hasura.Scalar.unwrapCodecs |> .codecUuid |> .decoder |> Decode.nullable)


name : SelectionSet (Maybe String) Api.Hasura.Object.Initiative_max_fields
name =
    Object.selectionForField "(Maybe String)" "name" [] (Decode.string |> Decode.nullable)


website : SelectionSet (Maybe String) Api.Hasura.Object.Initiative_max_fields
website =
    Object.selectionForField "(Maybe String)" "website" [] (Decode.string |> Decode.nullable)
