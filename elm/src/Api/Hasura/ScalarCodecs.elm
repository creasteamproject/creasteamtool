-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module Api.Hasura.ScalarCodecs exposing (..)

import Api.Hasura.Scalar exposing (defaultCodecs)
import Json.Decode as Decode exposing (Decoder)


type alias Uuid =
    Api.Hasura.Scalar.Uuid


codecs : Api.Hasura.Scalar.Codecs Uuid
codecs =
    Api.Hasura.Scalar.defineCodecs
        { codecUuid = defaultCodecs.codecUuid
        }
