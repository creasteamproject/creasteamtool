module View exposing (View, map, none, placeholder, toBrowserDocument)

import Browser
import Element exposing (..)
import Element.Border as B
import Gen.Route as Route


type alias View msg =
    { title : String
    , attributes : List (Element.Attribute msg)
    , element : Element msg
    }


placeholder : String -> View msg
placeholder str =
    { title = str
    , attributes = []
    , element = el [ centerX, centerY ] <| text str
    }


none : View msg
none =
    { title = ""
    , attributes = []
    , element = Element.none
    }


map : (a -> b) -> View a -> View b
map fn view =
    { title = view.title
    , attributes = view.attributes |> List.map (Element.mapAttribute fn)
    , element = Element.map fn view.element
    }


toBrowserDocument : View msg -> Browser.Document msg
toBrowserDocument view =
    { title = view.title
    , body =
        [ layout [] <|
            column [ width fill, height fill ]
                [ header
                , el ([ width fill, height fill ] ++ view.attributes) view.element
                , footer
                ]
        ]
    }


header : Element msg
header =
    row [ padding 10, spaceEvenly, width fill, B.widthEach { bottom = 1, left = 0, right = 0, top = 0 } ]
        [ el [] Element.none
        , el [] <| link [] { url = Route.toHref Route.Home_, label = text "Home" }
        , el [] <| link [] { url = Route.toHref Route.Search, label = text "Search" }
        , el [] <| link [] { url = Route.toHref Route.Admin, label = text "Admin" }
        , el [] Element.none
        ]


footer : Element msg
footer =
    row [ padding 10, spaceEvenly, width fill, B.widthEach { bottom = 0, left = 0, right = 0, top = 1 } ]
        [ el [] <| text "home"
        , el [] <| text "search"
        , el [] <| text "admin"
        ]
