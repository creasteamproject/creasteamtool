module Api exposing (GraphqlData, InitiativeDetailed, InitiativeSimple, deleteInitiative, getInitiativeDetails, insertInitiative, makeMutation, makeQuery, searchInitiatives, updateInitiative)

import Api.Hasura.InputObject as InputObject
import Api.Hasura.Mutation as Mutation
import Api.Hasura.Object as Obj
import Api.Hasura.Object.Area as ObjArea
import Api.Hasura.Object.Initiative as ObjInitiative
import Api.Hasura.Object.Initiative_area as ObjInitiativeArea
import Api.Hasura.Object.Initiative_tag as ObjInitiativeTag
import Api.Hasura.Object.Tag as ObjTag
import Api.Hasura.Query as Query
import Api.Hasura.Scalar as Scalar
import Graphql.Http
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import RemoteData exposing (RemoteData)



-- Selection Sets


insertInitiative : InputObject.Initiative_insert_input -> SelectionSet (Maybe Scalar.Uuid) RootMutation
insertInitiative object =
    let
        required_args =
            Mutation.InsertInitiativeOneRequiredArguments object
    in
    Mutation.insert_initiative_one
        Basics.identity
        required_args
        ObjInitiative.id


updateInitiative : (Mutation.UpdateInitiativeByPkOptionalArguments -> Mutation.UpdateInitiativeByPkOptionalArguments) -> Scalar.Uuid -> SelectionSet (Maybe Scalar.Uuid) RootMutation
updateInitiative optionalArgsFunc id =
    let
        pk_columns_input =
            InputObject.Initiative_pk_columns_input id

        required_args =
            Mutation.UpdateInitiativeByPkRequiredArguments pk_columns_input
    in
    Mutation.update_initiative_by_pk
        optionalArgsFunc
        required_args
        ObjInitiative.id


deleteInitiative : Scalar.Uuid -> SelectionSet (Maybe Scalar.Uuid) RootMutation
deleteInitiative id =
    let
        required_args =
            Mutation.DeleteInitiativeByPkRequiredArguments id
    in
    Mutation.delete_initiative_by_pk
        required_args
        ObjInitiative.id


searchInitiatives : String -> SelectionSet (List InitiativeSimple) RootQuery
searchInitiatives name =
    Query.initiative
        (\optionalArgs ->
            { optionalArgs
                | where_ =
                    Present <|
                        InputObject.buildInitiative_bool_exp
                            (\optionalFields ->
                                { optionalFields
                                    | name =
                                        Present <|
                                            InputObject.buildString_comparison_exp
                                                (\nameFields ->
                                                    { nameFields
                                                        | ilike_ = Present ("%" ++ name ++ "%")
                                                    }
                                                )
                                }
                            )
            }
        )
        initiativeSimpleSelectionSet


type alias InitiativeSimple =
    { id : Scalar.Uuid
    , name : String
    , descrtiption : String
    , website : String
    , tags_name : List String
    }


initiativeSimpleSelectionSet : SelectionSet InitiativeSimple Obj.Initiative
initiativeSimpleSelectionSet =
    SelectionSet.succeed
        InitiativeSimple
        |> SelectionSet.with ObjInitiative.id
        |> SelectionSet.with ObjInitiative.name
        |> SelectionSet.with ObjInitiative.description
        |> SelectionSet.with ObjInitiative.website
        |> SelectionSet.with (ObjInitiative.initiative_tags Basics.identity <| ObjInitiativeTag.tag ObjTag.name)


getInitiativeDetails : Scalar.Uuid -> SelectionSet (Maybe InitiativeDetailed) RootQuery
getInitiativeDetails id =
    let
        required_args =
            Query.InitiativeByPkRequiredArguments id
    in
    Query.initiative_by_pk
        required_args
        initiativeDetailedSelectionSet


type alias InitiativeDetailed =
    { id : Scalar.Uuid
    , name : String
    , descrtiption : String
    , website : String
    , contact : Maybe String
    , corporate_partners : Maybe String
    , funding_year : Maybe Int
    , funding_institution : Maybe String
    , tags_name : List String
    , areas_name : List String
    }


initiativeDetailedSelectionSet : SelectionSet InitiativeDetailed Obj.Initiative
initiativeDetailedSelectionSet =
    SelectionSet.succeed
        InitiativeDetailed
        |> SelectionSet.with ObjInitiative.id
        |> SelectionSet.with ObjInitiative.name
        |> SelectionSet.with ObjInitiative.description
        |> SelectionSet.with ObjInitiative.website
        |> SelectionSet.with ObjInitiative.contact
        |> SelectionSet.with ObjInitiative.corporate_partners
        |> SelectionSet.with ObjInitiative.funding_year
        |> SelectionSet.with ObjInitiative.funding_institution
        |> SelectionSet.with (ObjInitiative.initiative_tags Basics.identity <| ObjInitiativeTag.tag ObjTag.name)
        |> SelectionSet.with (ObjInitiative.initiative_areas Basics.identity <| ObjInitiativeArea.area ObjArea.name)



-- Graphql General Utils


type alias GraphqlData decodesTo =
    RemoteData (Graphql.Http.Error decodesTo) decodesTo


makeQuery : String -> (GraphqlData decodesTo -> msg) -> SelectionSet decodesTo RootQuery -> Cmd msg
makeQuery host msg selectionSet =
    selectionSet
        |> Graphql.Http.queryRequest ("http://" ++ host ++ ":8082/v1/graphql")
        |> Graphql.Http.send (RemoteData.fromResult >> msg)


makeMutation : String -> String -> (GraphqlData decodesTo -> msg) -> SelectionSet decodesTo RootMutation -> Cmd msg
makeMutation host secret msg selectionSet =
    selectionSet
        |> Graphql.Http.mutationRequest ("http://" ++ host ++ ":8082/v1/graphql")
        |> Graphql.Http.withHeader "x-hasura-admin-secret" secret
        |> Graphql.Http.send (RemoteData.fromResult >> msg)
