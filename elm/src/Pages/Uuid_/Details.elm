module Pages.Uuid_.Details exposing (Model, Msg, page)

import Api
import Api.Hasura.Scalar as Scalar
import Element exposing (..)
import Gen.Params.Uuid_.Details exposing (Params)
import Page
import RemoteData
import Request
import Shared
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared req =
    Page.element
        { init = init req
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- INIT


type alias Model =
    { host : String
    , uuid : Scalar.Uuid
    , data : Api.GraphqlData (Maybe Api.InitiativeDetailed)
    }


init : Request.With Params -> ( Model, Cmd Msg )
init req =
    ( { host = .host req.url
      , uuid = Scalar.Uuid <| .uuid req.params
      , data = RemoteData.Loading
      }
    , Api.makeQuery (.host req.url) Response <| Api.getInitiativeDetails <| Scalar.Uuid <| .uuid req.params
    )



-- UPDATE


type Msg
    = Response (Api.GraphqlData (Maybe Api.InitiativeDetailed))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Response data ->
            ( { model | data = data }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> View Msg
view model =
    { title = "Details"
    , attributes = []
    , element = viewData model.data
    }


viewData : Api.GraphqlData (Maybe Api.InitiativeDetailed) -> Element Msg
viewData data =
    case data of
        RemoteData.Success initiative ->
            viewDataSuccess initiative

        RemoteData.Failure error ->
            el [ centerX, centerY ] <| text "Failure"

        RemoteData.Loading ->
            el [ centerX, centerY ] <| text "Loading..."

        _ ->
            none


viewDataSuccess : Maybe Api.InitiativeDetailed -> Element Msg
viewDataSuccess maybeInitiative =
    case maybeInitiative of
        Just initiative ->
            viewInitiative initiative

        Nothing ->
            el [ centerX, centerY ] <| text "This initiative does not exist"


viewInitiative : Api.InitiativeDetailed -> Element Msg
viewInitiative initiative =
    el [ centerX, centerY ] <| text initiative.name
