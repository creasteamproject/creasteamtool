module Pages.Home_ exposing (view)

import Element exposing (..)
import View exposing (View)


view : View msg
view =
    { title = "Homepage"
    , attributes = []
    , element = el [ centerX, centerY ] <| text "Homepage"
    }
