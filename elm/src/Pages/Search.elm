module Pages.Search exposing (Model, Msg, page)

import Api
import Api.Hasura.Scalar as Scalar
import Element exposing (..)
import Element.Border as B
import Gen.Params.Search exposing (Params)
import Gen.Route as Route
import Page
import RemoteData
import Request
import Shared
import Url exposing (Url)
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared req =
    Page.element
        { init = init req.url
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- INIT


type alias Model =
    { host : String
    , data : Api.GraphqlData (List Api.InitiativeSimple)
    }


init : Url -> ( Model, Cmd Msg )
init url =
    ( { host = url.host, data = RemoteData.Loading }, Api.makeQuery url.host Response <| Api.searchInitiatives "" )



-- UPDATE


type Msg
    = Response (Api.GraphqlData (List Api.InitiativeSimple))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Response data ->
            ( { model | data = data }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> View Msg
view model =
    { title = "Search"
    , attributes = []
    , element = viewData model.data
    }


viewData : Api.GraphqlData (List Api.InitiativeSimple) -> Element Msg
viewData data =
    case data of
        RemoteData.Success initiatives ->
            viewDataSuccess initiatives

        RemoteData.Failure error ->
            el [ centerX, centerY ] <| text "Failure"

        RemoteData.Loading ->
            el [ centerX, centerY ] <| text "Loading..."

        _ ->
            none


viewDataSuccess : List Api.InitiativeSimple -> Element Msg
viewDataSuccess initiatives =
    wrappedRow [ padding 20, spacing 20 ] <|
        List.map initiativeCell initiatives


initiativeCell : Api.InitiativeSimple -> Element Msg
initiativeCell initiative =
    let
        uuid =
            case initiative.id of
                Scalar.Uuid string ->
                    string
    in
    el [ padding 30, B.width 1, B.rounded 10 ] <|
        link []
            { url =
                Route.toHref <|
                    Route.Uuid___Details { uuid = uuid }
            , label = text initiative.name
            }
