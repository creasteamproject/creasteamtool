{
  description = "Developement environment for Nix users";

  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, flake-utils, devshell, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: {
      devShell =
        let pkgs = import nixpkgs {
          inherit system;

          overlays = [ devshell.overlay ];
        };
        in
        pkgs.devshell.mkShell {
          imports = [ "${pkgs.devshell.extraModulesDir}/language/c.nix" ];
          commands = [
            {
              package = pkgs.devshell.cli;
              help = "Per project developer environments";
            }
            {
              package = pkgs.gitflow;
            }
            {
              package = pkgs.elmPackages.elm;
            }
            {
              package = pkgs.nodejs;
            }
            {
              name = "nim";
              package = pkgs.nim;
            }
            {
              package = (pkgs.python39Full.withPackages(p: with p; [
                pygments
                pandas
              ]));
            }
          ];
          devshell.packages = with pkgs; [
            elmPackages.elm-test
            elmPackages.elm-language-server
            elmPackages.elm-format
            elmPackages.elm-analyse
            elmPackages.elm-coverage
            elmPackages.elm-json
            elmPackages.elm-review
            nimlsp
          ];
          language.c.libraries = with pkgs; [
            libzip
          ];
        };
    });
}
