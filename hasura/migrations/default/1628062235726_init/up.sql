
SET check_function_bodies = false;

CREATE TABLE "public"."tag" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."initiative" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, "website" text NOT NULL, "description" text NOT NULL, "contact" text, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."initiative_tag" ("initiative_id" uuid NOT NULL, "tag_id" uuid NOT NULL, PRIMARY KEY ("initiative_id","tag_id") , FOREIGN KEY ("initiative_id") REFERENCES "public"."initiative"("id") ON UPDATE cascade ON DELETE cascade, FOREIGN KEY ("tag_id") REFERENCES "public"."tag"("id") ON UPDATE restrict ON DELETE restrict);

alter table "public"."initiative" add column "funding_institution" text
 null;

alter table "public"."initiative" add column "corporate_partners" text
 null;

alter table "public"."initiative" add column "funding_year" Integer
 null;

CREATE TABLE "public"."area" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"), UNIQUE ("name"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."tag" add constraint "tag_name_key" unique ("name");

CREATE TABLE "public"."initiative_area" ("initiative_id" uuid NOT NULL, "area_id" uuid NOT NULL, PRIMARY KEY ("initiative_id","area_id") , FOREIGN KEY ("initiative_id") REFERENCES "public"."initiative"("id") ON UPDATE cascade ON DELETE cascade, FOREIGN KEY ("area_id") REFERENCES "public"."area"("id") ON UPDATE restrict ON DELETE restrict);
