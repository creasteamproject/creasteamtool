
DROP TABLE "public"."initiative_area";

alter table "public"."tag" drop constraint "tag_name_key";

DROP TABLE "public"."area";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."initiative" add column "funding_year" Integer
--  null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."initiative" add column "corporate_partners" text
--  null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."initiative" add column "funding_institution" text
--  null;

DROP TABLE "public"."initiative_tag";

DROP TABLE "public"."initiative";

DROP TABLE "public"."tag";
